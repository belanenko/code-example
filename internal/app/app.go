package app

import (
	"context"
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog/log"
	"gitlab.com/cs.watch/backend/stats-service/internal/config"
	"gitlab.com/cs.watch/backend/stats-service/internal/controller/http/common"
	"gitlab.com/cs.watch/backend/stats-service/internal/services"
	"gitlab.com/cs.watch/backend/stats-service/internal/usecase"
)

func Run(conf *config.AppConfig) error {
	services, err := services.CreateServices(config.Config)
	if err != nil {
		return fmt.Errorf("can't create services: %v", err)
	}

	usecases := usecase.NewStatUsecase(*services)
	handler := usecase.NewHandler(
		usecases.Queue,
		usecases.Info,
		usecases.File,
	)

	ctx := context.Background()
	notify := make(chan error, 1)

	server := CreateServer(&conf.HTTPServer)
	go func() {
		err := server.Listen(fmt.Sprintf(":%d", conf.HTTPServer.ListenPort))
		if err != nil {
			notify <- err
		}
	}()

	for {
		select {
		case <-ctx.Done():
			log.Info().Msg("Context done")
			return nil
		case err := <-notify:
			log.Err(err).Msg("Received notify error")
		default:
			msg, err := usecases.Queue.Consume(ctx)
			if err != nil {
				log.Err(err).Msg("consume msg")
			}
			err = handler.Handle(ctx, msg)
			if err != nil {
				log.Err(err).Msg("handle msg")
			}
		}
	}
}

func CreateServer(config *config.HTTPServer) *fiber.App {
	app := fiber.New(fiber.Config{
		BodyLimit: config.BodyLimit,
	})
	common.Register(app)
	return app
}
