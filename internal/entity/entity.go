package entity

import amqp "github.com/rabbitmq/amqp091-go"

type GameStatInfo struct {
	Map              string  `json:"map"`
	UserTeamScore    int     `json:"user_team_score"`
	EnemyTeamScore   int     `json:"enemy_team_score"`
	KillsCount       int     `json:"kills_count"`
	DeathCount       int     `json:"death_count"`
	OpenFrag         int     `json:"open_kills_count"`
	FirstDeaths      int     `json:"open_death_count"`
	AssistsCount     int     `json:"assists_count"`
	HeadshotsPercent int     `json:"headshots_percent"`
	Kd               float64 `json:"kd"`
	AvgDamage        int     `json:"avg_damage"`
	GrenadeDamage    int     `json:"grenade_damage"`
	FlashbangGrenade int     `json:"flash_count"`
	SmokeGrenade     int     `json:"smoke_count"`
	MolotovGrenade   int     `json:"moly_count"`
	HeGrenade        int     `json:"he_grenade_count"`
	Clutches         []int   `json:"clutches_count"`
}

// Raw msg example:
// {"msg_id":1,"steam_id64":76561198096203666,"demo_url":"https://storage.yandexcloud.net/testss3/match730_003581654972099461326_1016781245_272.dem"}
type Message struct {
	MsgID    int64  `json:"msg_id"`
	SteamID  int64  `json:"steam_id64"`
	DemoURL  string `json:"demo_url"`
	QueueMsg *amqp.Delivery
}

func (cm *Message) Ack() error {
	return cm.QueueMsg.Ack(false)
}

func (cm *Message) Nack() error {
	return cm.QueueMsg.Nack(false, true)
}

type Result struct {
	MsgID  int64         `json:"msg_id"`
	Status string        `json:"status"`
	Info   *GameStatInfo `json:"info,omitempty"`
	Error  string        `json:"error,omitempty"`
}
