package usecase

import (
	"context"

	"github.com/rs/zerolog/log"
)

type FileManagerUsecase interface {
	Download(ctx context.Context, url string) (string, error)
}

type FileManager struct {
	downloader FileManagerUsecase
}

func NewFileUsecase(fu FileManagerUsecase) *FileManager {
	return &FileManager{
		downloader: fu,
	}
}

func (f *FileManager) Download(ctx context.Context, url string) (string, error) {
	path, err := f.downloader.Download(ctx, url)
	if err != nil {
		return "", err
	}
	log.Info().Msgf("File successful downloaded: %s", url)
	return path, nil
}
