package usecase

import "gitlab.com/cs.watch/backend/stats-service/internal/services"

type StatUsecase struct {
	Queue QueueManagerUsecase
	File  FileManagerUsecase
	Info  GameStatUsecase
}

func NewStatUsecase(services services.Services) *StatUsecase {
	return &StatUsecase{
		Queue: NewQueueUsecase(services.QueueService),
		File:  NewFileUsecase(services.FileService),
		Info:  NewGameStatUsecase(services.StatService),
	}
}
