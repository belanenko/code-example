package usecase

import (
	"context"

	"github.com/rs/zerolog/log"
	"gitlab.com/cs.watch/backend/stats-service/internal/entity"
)

type QueueManagerUsecase interface {
	Consume(ctx context.Context) (*entity.Message, error)
	Publish(context.Context, *entity.Result) error
}

type QueueUsecase struct {
	queue QueueManagerUsecase
}

func NewQueueUsecase(queue QueueManagerUsecase) *QueueUsecase {
	return &QueueUsecase{
		queue: queue,
	}
}

func (q *QueueUsecase) Consume(ctx context.Context) (*entity.Message, error) {
	msg, err := q.queue.Consume(ctx)
	if err != nil {
		log.Err(err).Msgf("Failed to consume message")
		return nil, err
	}
	log.Info().Msgf("Received message id: %s", msg.MsgID)
	return msg, nil
}

func (q *QueueUsecase) Publish(ctx context.Context, msg *entity.Result) error {
	err := q.queue.Publish(ctx, msg)
	if err != nil {
		log.Err(err).Msgf("Failed to publish message: %s", msg.MsgID)
		return err
	}

	log.Info().Msgf("Published message: %s", msg.MsgID)
	return nil
}
