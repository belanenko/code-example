package usecase

import (
	"context"
	"os"

	"github.com/rs/zerolog/log"
	"gitlab.com/cs.watch/backend/stats-service/internal/entity"
)

type HandlerUsecase interface {
	Handle(ctx context.Context, msg *entity.Message) error
}

type Handler struct {
	queue QueueManagerUsecase
	stat  GameStatUsecase
	file  FileManagerUsecase
}

func NewHandler(queue QueueManagerUsecase, stat GameStatUsecase, file FileManagerUsecase) *Handler {
	return &Handler{
		queue: queue,
		stat:  stat,
		file:  file,
	}
}

func (h Handler) Handle(ctx context.Context, msg *entity.Message) error {
	result := &entity.Result{
		MsgID: msg.MsgID,
	}

	defer func() {
		if result.Status == "successful" || result.Status == "error" {
			msg.Ack()
		} else {
			msg.Nack()
		}
	}()

	if msg.SteamID == 0 {
		result.Status = "error"
		result.Error = "steam_id64 is zero"
		return nil
	}

	path, err := h.file.Download(ctx, msg.DemoURL)
	if err != nil {
		log.Err(err).Msgf("can't get file: %v", err)
		result.Status = "error"
		result.Error = err.Error()
		return err
	}
	defer os.Remove(path)

	info, err := h.stat.GetGameInfoBySteamID64(uint64(msg.SteamID), path)
	if err != nil {
		log.Err(err).Msg("can't get game info")
		result.Status = "error"
		result.Error = err.Error()
		return err
	}
	result.Info = info
	result.Status = "stats_done"

	err = h.queue.Publish(ctx, result)
	if err != nil {
		log.Err(err).Msg("can't publish message")
		return err
	}

	return nil
}
