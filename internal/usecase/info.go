package usecase

import (
	"gitlab.com/cs.watch/backend/stats-service/internal/entity"
)

type GameStatUsecase interface {
	GetGameInfoBySteamID64(userSteamID64 uint64, path string) (*entity.GameStatInfo, error)
}

type GameStat struct {
	info GameStatUsecase
}

func NewGameStatUsecase(info GameStatUsecase) *GameStat {
	return &GameStat{
		info: info,
	}
}

func (ss *GameStat) GetGameInfoBySteamID64(userSteamID64 uint64, path string) (*entity.GameStatInfo, error) {
	return ss.info.GetGameInfoBySteamID64(userSteamID64, path)
}
