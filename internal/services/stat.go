package services

import (
	"os"

	"github.com/markus-wa/demoinfocs-golang/v3/pkg/demoinfocs"
	"github.com/markus-wa/demoinfocs-golang/v3/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v3/pkg/demoinfocs/events"
	"gitlab.com/cs.watch/backend/stats-service/internal/entity"
)

type StatService struct {
}

func NewStatService() *StatService {
	return &StatService{}
}

func (ss *StatService) GetGameInfoBySteamID64(userSteamID64 uint64, path string) (*entity.GameStatInfo, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	p := demoinfocs.NewParser(f)
	defer p.Close()

	headers, err := p.ParseHeader()
	if err != nil {
		return nil, err
	}

	info := &entity.GameStatInfo{
		Map: headers.MapName,
	}

	matchIsStarted := false
	hasKillOnRound := false
	headshots := 0
	damageSum := 0

	p.RegisterEventHandler(func(e events.RoundStart) {
		matchIsStarted = true
	})

	p.RegisterEventHandler(func(e events.RoundStart) {
		hasKillOnRound = false // Флаг который обнуляется для расчета опенфрагов
	})

	p.RegisterEventHandler(func(e events.Kill) {
		if !matchIsStarted {
			return
		}
		if e.Killer != nil && userSteamID64 == e.Killer.SteamID64 {
			if e.Victim != nil && UserInTeam(userSteamID64, e.Victim.TeamState.Members()) {
				info.KillsCount-- // Подсчет убийств если мы убили союзника
			} else {
				info.KillsCount++ // Подсчет убийств

				if e.IsHeadshot {
					headshots++ // Подсчет хедшотов
				}

				if !hasKillOnRound {
					info.OpenFrag++ // Подсчет опенфрагов, если наш килл первый
				}
			}
		}

		if e.Assister != nil && userSteamID64 == e.Assister.SteamID64 {
			info.AssistsCount++ // Подсчет ассистов
		}

		if e.Victim != nil && userSteamID64 == e.Victim.SteamID64 {
			info.DeathCount++ // Подсчет смертей

			if !hasKillOnRound {
				info.FirstDeaths++ // Подсчет первых смертей
			}
		}

		hasKillOnRound = true
	})

	p.RegisterEventHandler(func(e events.PlayerHurt) {
		if !matchIsStarted {
			return
		}
		if e.Attacker != nil && e.Player != nil && e.Attacker.SteamID64 == userSteamID64 && !UserInTeam(userSteamID64, e.Player.TeamState.Members()) {
			damageSum += e.HealthDamageTaken // Подсчет нанесенного урона

			if WeaponIsGrenade(e.Weapon.Type) {
				info.GrenadeDamage += e.HealthDamageTaken // Подсчет нанесенного урона гранатами
			}
		}
	})

	p.RegisterEventHandler(func(e events.RoundEnd) {
		if !matchIsStarted {
			return
		}
		if e.WinnerState != nil && UserInTeam(userSteamID64, e.WinnerState.Members()) {
			info.UserTeamScore++ // Подсчет счета матча
		}
		if e.LoserState != nil && UserInTeam(userSteamID64, e.LoserState.Members()) {
			info.EnemyTeamScore++ // Подсчет счета матча
		}
	})

	p.RegisterEventHandler(func(e events.GrenadeProjectileDestroy) {
		if !matchIsStarted {
			return
		}
		if e.Projectile.Thrower.SteamID64 == userSteamID64 {
			grenadeType := e.Projectile.WeaponInstance.Type
			switch grenadeType { // Подсчет кол-ва кинутых гранат
			case common.EqDecoy:
				info.FlashbangGrenade++
			case common.EqMolotov, common.EqIncendiary:
				info.MolotovGrenade++
			case common.EqSmoke:
				info.SmokeGrenade++
			case common.EqHE:
				info.HeGrenade++
			}

		}
	})

	err = p.ParseToEnd()
	if err != nil {
		return nil, err
	}

	if info.DeathCount == 0 {
		info.Kd = float64(info.KillsCount)
	} else {
		info.Kd = float64(info.KillsCount) / float64(info.DeathCount) // Рассчет КД
	}
	if info.UserTeamScore+info.EnemyTeamScore != 0 {
		info.AvgDamage = damageSum / (info.UserTeamScore + info.EnemyTeamScore) // Рассчет среднего урона в раунде
	}

	if info.KillsCount == 0 {
		info.HeadshotsPercent = 0
	} else {
		info.HeadshotsPercent = int(float64(headshots) / float64(info.KillsCount) * 100) // Рассчет процента хедшотов
	}

	return info, nil
}

func UserInTeam(userSteamID64 uint64, teamPlayers []*common.Player) bool {
	for _, player := range teamPlayers {
		if uint64(player.SteamID64) == userSteamID64 {
			return true
		}
	}

	return false
}

func WeaponIsGrenade(weaponType common.EquipmentType) bool {
	grenades := map[common.EquipmentType]any{
		common.EqDecoy:      nil,
		common.EqMolotov:    nil,
		common.EqIncendiary: nil,
		common.EqFlash:      nil,
		common.EqSmoke:      nil,
		common.EqHE:         nil,
	}
	_, isGrenade := grenades[weaponType]
	return isGrenade
}
