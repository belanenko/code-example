package services

import "gitlab.com/cs.watch/backend/stats-service/internal/config"

type Services struct {
	QueueService *QueueService
	FileService  *FileService
	StatService  *StatService
}

func CreateServices(conf *config.AppConfig) (*Services, error) {
	queueService, err := NewQueueService(conf.DSN.RabbitMQ)
	if err != nil {
		return nil, err
	}
	services := &Services{
		QueueService: queueService,
		FileService:  NewFileService(),
		StatService:  NewStatService(),
	}

	return services, err
}
