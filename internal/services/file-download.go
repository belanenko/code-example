package services

import (
	"context"
	"io"
	"net/http"
	"os"
)

type FileService struct {
}

func (ds *FileService) Download(ctx context.Context, url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	f, err := os.CreateTemp("", "")
	if err != nil {
		return "", err
	}
	defer f.Close()
	_, err = io.Copy(f, resp.Body)
	if err != nil {
		return "", err
	}

	return f.Name(), nil
}

func NewFileService() *FileService {
	return &FileService{}
}
