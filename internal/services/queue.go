package services

import (
	"context"
	"encoding/json"
	"fmt"

	info "github.com/matishsiao/goInfo"
	"github.com/rabbitmq/amqp091-go"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/cs.watch/backend/stats-service/internal/entity"
)

type QueueService struct {
	conn        *amqp.Connection
	channel     *amqp.Channel
	queueStat   *amqp.Queue
	queueResult *amqp.Queue
	chanMsgs    <-chan amqp.Delivery
}

func NewQueueService(connstr string) (*QueueService, error) {
	config := amqp.Config{Properties: amqp.NewConnectionProperties()}
	var clientName string
	info, err := info.GetInfo()
	if err != nil {
		clientName = "stat-service"
	} else {
		clientName = fmt.Sprintf("stat-service-%s", info.String())
	}

	config.Properties.SetClientConnectionName(clientName)
	conn, err := amqp091.DialConfig(connstr, config)
	if err != nil {
		return nil, err
	}

	qs := &QueueService{
		conn: conn,
	}

	err = qs.setup()
	if err != nil {
		return nil, err
	}

	err = qs.startConsume()
	if err != nil {
		return nil, err
	}
	return qs, nil
}

func (qs *QueueService) Close() {
	qs.conn.Close()
}

func (qs *QueueService) setup() error {
	channel, err := qs.conn.Channel()
	if err != nil {
		return err
	}
	qs.channel = channel

	queueStat, err := channel.QueueDeclare("stats-queue", false, false, false, false, nil)
	if err != nil {
		return err
	}
	qs.queueStat = &queueStat

	queueResult, err := channel.QueueDeclare("stats-result", false, false, false, false, nil)
	if err != nil {
		return err
	}
	qs.queueResult = &queueResult

	return err
}

func (qs *QueueService) startConsume() error {
	msgs, err := qs.channel.Consume(
		qs.queueStat.Name,
		"stat-service",
		false, // auto-ack
		false, // exclusive
		false, // no-local
		false, // no-wait
		nil,   // args
	)
	if err != nil {
		return err
	}
	qs.chanMsgs = msgs
	return nil
}

func (qs *QueueService) Consume(ctx context.Context) (*entity.Message, error) {
	msg, ok := <-qs.chanMsgs

	if !ok {
		return nil, fmt.Errorf("message channel is closed")
	}
	cm := &entity.Message{
		QueueMsg: &msg,
	}
	err := json.Unmarshal(msg.Body, cm)
	if err != nil {
		return nil, err
	}

	return cm, nil
}

func (qs *QueueService) Publish(ctx context.Context, msg *entity.Result) error {
	body, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	err = qs.channel.PublishWithContext(
		ctx,
		"",                  // exchange
		qs.queueResult.Name, // routing key
		false,               // mandatory
		false,               // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte(body),
		},
	)

	return err
}
