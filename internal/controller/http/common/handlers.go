package common

import (
	"github.com/gofiber/fiber/v2"
)

// Healthcheck проверяет состояние сервиса
// @Description Проверяет состояние сервиса
// @Summary Проверяет состояние сервиса
// @Tags common
// @Success 200
// @ID healthcheck
// @Router /healthcheck [get]
func (r Router) Healthcheck(ctx *fiber.Ctx) error {
	return ctx.SendStatus(fiber.StatusOK)
}
