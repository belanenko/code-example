package common

import (
	"github.com/gofiber/fiber/v2"
	fiberSwagger "github.com/gofiber/swagger"
	"gitlab.com/cs.watch/backend/stats-service/docs/swagger/common"
)

//go:generate go install github.com/swaggo/swag/cmd/swag@v1.8.12
//go:generate swag init -o ./../../../../docs/swagger/common -g router.go --parseInternal --parseDependency --instanceName common

// @Title Backend service common API
// @Version 1.0
// @Description Backend for main cs service
// @BasePath /

type Router struct{}

func Register(server *fiber.App) {
	router := &Router{}
	handler := fiber.New(server.Config())

	handler.Get("/healthcheck", router.Healthcheck)

	swagger := fiberSwagger.New(fiberSwagger.Config{InstanceName: common.SwaggerInfocommon.InstanceName()})
	server.Get("/swagger/common/*", swagger)
	server.Mount("/", handler)
}
