package config

import (
	"github.com/caarlos0/env/v7"
)

type DSN struct {
	RabbitMQ string `env:"RABBITMQ,notEmpty"`
}

type HTTPServer struct {
	ListenPort int `env:"LISTEN_PORT" validate:"required,hostname_port" example:"0.0.0.0:8080"`
	BodyLimit  int `env:"BODY_LIMIT" validate:"gte=1024,lte=10485760" example:"4194304"`
}

type AppConfig struct {
	DSN        DSN `envPrefix:"STAT_DSN_"`
	HTTPServer `envPrefix:"STAT_HTTP_SERVER_"`
}

var Config *AppConfig

func Init() error {
	Config = &AppConfig{}
	return env.Parse(Config)
}
