# Build stage
FROM golang:1.20.2-alpine AS builder
WORKDIR /build
# Install build dependencies
RUN apk add --no-cache gcc musl-dev
# Copy go.mod and go.sum files
COPY go.mod go.sum ./
# Download dependencies
RUN go mod download
# Copy the source code
COPY . .
# Build the Go application
RUN go build -o stat ./cmd/main.go

# Runtime stage
FROM alpine:3.15.0
WORKDIR /app
# Install CA certificates
RUN apk --no-cache add ca-certificates
# Copy the binary from the build stage
COPY --from=builder /build/stat /app/stat
# Set the entry point
ENTRYPOINT [ "/app/stat" ]
