package main

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/cs.watch/backend/stats-service/internal/app"
	"gitlab.com/cs.watch/backend/stats-service/internal/config"
)

func main() {
	err := config.Init()
	if err != nil {
		log.Err(err).Msgf("Failed init config")
	}

	err = app.Run(config.Config)
	if err != nil {
		log.Err(err).Msgf("Failed run app")
	}
}
